import logging
import argparse
from PIL import Image, ImageOps
from omegaconf import OmegaConf
from pathlib import Path
import json
import torch
from tqdm import tqdm

from models import Classifier
from data.transform import get_transform

class InferenceBioposh():
    def __init__(self, cf):
        super().__init__()
        self.cf = cf
        self.crit_dict = {
            'skin_type':2,
            'humidity':2,
            'pore':3,
            'blemish':3,
            'acne':3,
            'symptom':4
        }
        self.transform = get_transform(cf, mode='test')
        self.criteria = cf.data.criteria.strip(' ').split(',')
        num_classes = [self.crit_dict[criterion] for criterion in self.criteria]
        self.model = Classifier(self.cf, num_classes)
        self.model.eval()

    def _load_image(self, path):
        if not path.exists():
            raise FileNotFoundError
        else:
            if self.cf.preprocess_mode.channels == 1:
                return ImageOps.exif_transpose(Image.open(path).convert('L'))  # read grayscale images
            elif self.cf.preprocess_mode.channels == 3:
                return ImageOps.exif_transpose(Image.open(path).convert("RGB"))
            else:  # FIXME: It may be unsafe. You may raise error.
                return ImageOps.exif_transpose(Image.open(path))
    
    
    def load_test(self, front, left, right):
        image_sample = list()

        front_sample = self._load_image(front)
        front_sample = self.transform(front_sample)
        image_sample.append(front_sample)

        left_sample = self._load_image(left)
        left_sample = self.transform(left_sample)
        image_sample.append(left_sample)

        right_sample = self._load_image(right)
        right_sample = self.transform(right_sample)
        image_sample.append(right_sample)

        return image_sample
    
    def forward(self, image_root, json):
        try:
            torch.cuda.set_device(self.cf.model.gpu_id)
            image_root = Path(image_root)
            img_name_list = [x.name for x in (image_root / 'front').iterdir() if x.suffix.lower() in ['.jpg', '.jpeg', '.png']]

            label_items = json['items']
            result_dict = {x:0 for x in self.criteria}
            result_dict['Num_sample'] = 0

            for img_name in tqdm(img_name_list, total=len(img_name_list)):
                front = image_root / 'front' / img_name
                left = image_root / 'left' / img_name
                right = image_root / 'right' / img_name

                inputs = self.load_test(front, left, right)
                inputs = [x.cuda() for x in inputs]

                # Model inference
                outputs = self.model(inputs)
                for ii, output in enumerate(outputs):
                    cur_crit = self.criteria[ii]

                    assert (label_items['front/'+img_name][cur_crit] == label_items['left/'+img_name][cur_crit]) and\
                                (label_items['front/'+img_name][cur_crit] == label_items['right/'+img_name][cur_crit]), 'Label Mismatch'

                    label = label_items['front/'+img_name][cur_crit]
                    _, pred = torch.max(output, 1)

                    if pred == label:
                        result_dict[cur_crit] += 1
                
                result_dict['Num_sample'] += 1


            for key in self.crit_dict.keys():
                result_dict[key] /= result_dict['Num_sample']

            return result_dict

        except Exception as e:  # logging when an exception occurs
            logging.exception(e)

def get_args():
    parser = argparse.ArgumentParser(
        description='Bioposh Classification Inference')
    parser.add_argument('-i', '--input_root',
                        nargs='?',
                        dest='input_root',
                        help='input path',
                        type=str,
                        required=True)
    parser.add_argument('-j', '--json_root',
                        nargs='?',
                        dest='json_root',
                        help='label json file directory',
                        type=str,
                        required=True)
    parser.add_argument('-y', '--yaml',
                        nargs='?',
                        dest='yaml',
                        help='yaml file directory',
                        type=str,
                        default='./configs/inference.yaml')
    parser.add_argument('-o', '--output_dir',
                        nargs='?',
                        dest='output_dir',
                        help='text output directory',
                        type=str,
                        default='.')
    parser.add_argument('-g', '--gpuid',
                        nargs='?',
                        help='GPU ID Selection',
                        type=int,
                        default=0)

    args = parser.parse_args()
    return args

def show_acc(result):
    output = ''
    for key in result.keys():
        if key != 'Num_sample':
            message = f'Class: {key}\tAccuracy: {result[key]*100} %\n'
            print(message)
            output += message
    message = f'Total sample number: {result["Num_sample"]} pics'
    output += message
    return output

def main():
    args = get_args()
    yaml = OmegaConf.load(args.yaml)
    yaml.model.gpu_id = args.gpuid

    logging.basicConfig(
        level=getattr(logging, 'INFO'),
        format="[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s",
    )

    Infer = InferenceBioposh(yaml)

    with open(args.json_root, 'r', encoding='utf-8') as rf:
        label_json = json.load(rf)

    output = Infer.forward(args.input_root, label_json)

    text = show_acc(output)
    with open(Path(args.output_dir) / 'result.txt', 'w', encoding='utf-8') as wf:
        wf.write(text)

if __name__=='__main__':
    main()