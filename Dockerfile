FROM docker.maum.ai:443/brain/vision:cu101

COPY ./requirements.txt /workspace/
WORKDIR /workspace

RUN pip install -r requirements.txt

EXPOSE 6999
#RUN sed -i 's/\r//' run.sh
#
#ENV DEVICE_ID 0
#ENV TRAIN false
#ENV INFERENCE false
#ENV CONFIGS /workspace/configs/classification.yaml
#
#ENTRYPOINT bash run.sh ${DEVICE_ID} ${TRAIN} ${INFERENCE} ${CONFIGS}
