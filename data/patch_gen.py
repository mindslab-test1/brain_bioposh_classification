import sys
sys.path.insert(0, "..")
import face_alignment.api as face_alignment
import numpy as np
import torch
from PIL import Image
import cv2

FA_MODEL = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D)

def resize_image(image_pil, target_size=720):
    w, h = image_pil.size
    ratio = h / float(w)
    if ratio > 1:  # h > w
        scale = w / float(target_size)
        to_w = target_size
        to_h = int(target_size * ratio)
    else:
        scale = h / float(target_size)
        to_h = target_size
        to_w = int(target_size / ratio)
    
    image_pil_new = image_pil.resize((to_w, to_h), Image.BICUBIC)
    return np.array(image_pil_new), scale

def get_kp(image, fa_model=None):
    with torch.no_grad():
        image_resized, scale = resize_image(Image.fromarray(image))
        landmark = fa_model.get_landmarks(image_resized)
    landmark = np.array(landmark[0])
    landmark *= scale

    return landmark


def subpatching(img, patch_size):
    if isinstance(patch_size, int):
        size = [patch_size, patch_size]
    elif isinstance(patch_size, list):
        size= patch_size
    else:
        raise TypeError(f"Invalid patch size type: {type(patch_size)}")
    img2 = Image.fromarray(img.copy())
    W, H = img2.size

    w_size = W//size[0]
    h_size = H//size[1]

    patches = []

    for wgrid in range(0, W, w_size):
        if wgrid > W-w_size:
            continue
        else:
            for hgrid in range(0, H, h_size):
                if hgrid > H-h_size:
                    continue
                else:
                    cropped = img2.crop((wgrid, hgrid, wgrid+w_size, hgrid+h_size)).resize((W,H))
                    patches.append(cropped)
    return patches


def make_rec(img, kps, patch_size):
    output = []
    height, width, _ = img.shape

    for cnt in kps:
        img2 = img.copy()
        xmin = sorted(cnt, key= lambda x : x[0])[0][0]
        xmax = sorted(cnt, key= lambda x : x[0])[-1][0]
        ymin = sorted(cnt, key= lambda x : x[1])[0][1]
        ymax = sorted(cnt, key= lambda x : x[1])[-1][1]

        src_pts = np.array([[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin]], dtype='float32')

        dst_pts = np.array([[0, 0], [0, height-1], [width - 1, height-1], [width - 1, 0]], dtype='float32')

        M = cv2.getPerspectiveTransform(src_pts, dst_pts)

        warped = cv2.warpPerspective(img2, M, (width, height))

        patches = subpatching(warped, patch_size)

        output += patches

    return output


def separate_front(img, landmark, patch_size):
    eye_left = []
    eye_right = []

    cheek_left = []
    cheek_right = []

    jaw = []
    forehead = []

    el_idx = list(range(17, 22)) + list(range(36, 40))
    er_idx = list(range(22, 27)) + list(range(42, 46))

    cl_idx = list(range(1, 5)) + list(range(27, 31)) + [48]
    cr_idx = list(range(12, 16)) + list(range(27, 31)) + [54]

    jaw_idx = [48] + list(range(54, 60)) + list(range(4, 13))

    forehead_idx = list(range(17, 27))

    for ii, pnt in enumerate(landmark):
        if ii in el_idx:
            eye_left.append(pnt)
        if ii in er_idx:
            eye_right.append(pnt)
        if ii in cl_idx:
            cheek_left.append(pnt)
        if ii in cr_idx:
            cheek_right.append(pnt)
        if ii in jaw_idx:
            jaw.append(pnt)
        if ii in forehead_idx:
            forehead.append(pnt)

    ld = forehead[0]
    rd = forehead[-1]
    length = (rd[0] - ld[0])//2
    lu = [ld[0], np.max([ld[1] - length, 0])]
    ru = [rd[0], np.max([rd[1] - length, 0])]
    forehead.extend([ru, lu])

    parts = [np.array(eye_left).astype(np.int32),
        np.array(eye_right).astype(np.int32),
        np.array(cheek_left).astype(np.int32),
        np.array(cheek_right).astype(np.int32),
        np.array(jaw).astype(np.int32),
        np.array(forehead).astype(np.int32)]
    
    output = make_rec(img, parts, patch_size)

    return output

def separate_left(img, landmark, patch_size):
    cheek = []
    nose = []
    forehead = []

    '''
    jaw
    nose
    lip
    eye
    eyebrow

    left_idx = list(range(8, 17)) \
                + list(range(27, 31)) + list(range(33, 36)) \
                + list(range(54, 58)) \
                + list(range(42, 48)) \
                + list(range(22, 27))
    '''
    cheek_idx = list(range(8, 17)) + [26, 35, 45, 54]
    nose_idx = list(range(27, 31)) + [35, 42]
    forehead_idx = list(range(22, 27))

    for ii, pnt in enumerate(landmarks):
        if ii in cheek_idx:
            cheek.append(pnt)
        if ii in nose_idx:
            nose.append(pnt)
        if ii in forehead_idx:
            forehead.append(pnt)
    
    ld = forehead[0]
    rd = forehead[-1]
    length = (rd[0] - ld[0])
    lu = [ld[0], np.max([ld[1] - length, 0])]
    ru = [rd[0], np.max([rd[1] - length, 0])]
    forehead.extend([ru, lu])
    
    parts = [np.array(cheek).astype(np.int32),
        np.array(nose).astype(np.int32),
        np.array(forehead).astype(np.int32)]
    
    output = make_rec(img, parts, patch_size)
    
    return output

def separate_right(img, landmarks, patch_size):
    cheek = []
    nose = []
    forehead = []

    '''
    right_idx = list(range(0, 9)) \
                + list(range(27, 34)) \
                + list(range(57, 60)) + [48] \
                + list(range(36, 42)) \
                + list(range(17, 22))
    '''
    cheek_idx = list(range(0,9)) + [17, 31, 36, 48]
    nose_idx = list(range(27, 32)) + [40]
    forehead_idx = list(range(17, 22))

    for ii, pnt in enumerate(landmarks):
        if ii in cheek_idx:
            cheek.append(pnt)
        if ii in nose_idx:
            nose.append(pnt)
        if ii in forehead_idx:
            forehead.append(pnt)
    
    ld = forehead[0]
    rd = forehead[-1]
    length = (rd[0] - ld[0])
    lu = [ld[0], np.max([ld[1] - length, 0])]
    ru = [rd[0], np.max([rd[1] - length, 0])]
    forehead.extend([ru, lu])
    
    parts = [np.array(cheek).astype(np.int32),
        np.array(nose).astype(np.int32),
        np.array(forehead).astype(np.int32)]
    
    output = make_rec(img, parts, patch_size)
    
    return output

def generate_patches(img, view, landmarks=None, patch_size=[3,3]):
    img_np = np.array(img)
    
    if landmarks is None:
        landmarks = get_kp(img_np, FA_MODEL)

    if view == 'front':
        patches = separate_front(img_np, landmarks, patch_size)

    elif view == 'left':
        patches = separate_left(img_np, landmarks, patch_size)

    elif view == 'right':
        patches = separate_right(img_np, landmarks, patch_size)

    else:
        raise ValueError(f"Invalid view: {view}")
    
    return patches



