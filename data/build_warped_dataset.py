from transform import __gaussian_blur, __random_warp, __salt_and_pepper, __perspective_transform
from PIL import Image, ImageOps, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from torchvision import transforms
from pathlib import Path
import os
from time import time
from tqdm import tqdm
import numpy as np


def main():
    test_img = Path('/workspace/dataset')
    views = ['front', 'left', 'right']

    # time_resized = []

    for view in views:
        current_dir = test_img / view
        img_list = [x for x in current_dir.iterdir() if x.name.split('.')[-1].lower() in ['jpg', 'jpeg', 'png']]
        for img in tqdm(img_list, total=len(img_list)):
            img_pil = ImageOps.exif_transpose(Image.open(img).resize((225,225)))
            start = time()
            img_warped = __random_warp(img_pil, None)
            time_resized.append(time() - start)
    print(f"RESIZE time consumed average: {np.mean(np.array(time_resized))}")


    # time_org = []
    
    # for view in views:
    #     current_dir = test_img / view
    #     img_list = [x for x in current_dir.iterdir() if x.name.split('.')[-1].lower() in ['jpg', 'jpeg', 'png']]
    #     for img in tqdm(img_list, total=len(img_list)):
    #         print(img.name)
    #         img_pil = ImageOps.exif_transpose(Image.open(img))
    #         start = time()
    #         img_warped = __random_warp(img_pil)
    #         time_org.append(time() - start)
    # print(f"ORIGINAL time consumed average: {np.mean(np.array(time_org))}")



if __name__ == '__main__':
    main()
    