r'''
    Author: Sungguk Cha
    Editor: Jinwoo Kim (21.01.19), Hyoung-Kyu Song (21.02.24)
    Custom dataset loader
'''

import torch
from PIL import Image, ImageOps
from pathlib import Path
import json
import logging
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

import torchvision.transforms as transforms
from .transform import get_transform, __transform_apply_mode#
from .transform import  __random_warp as RandomWarp#
logger = logging.getLogger('bioposh')
import numpy as np
from .patch_gen import generate_patches#
from random import choice, sample, random

from prefetch_generator import BackgroundGenerator

class DatasetBack(Dataset):
    def __init__(self, *args, **kwargs):
        super(DatasetBack, self).__init__(*args, **kwargs)
    def __iter__(self):
        return BackgroundGenerator(super().__iter__())

class BioposhDataset(DatasetBack):
    def __init__(self, hp, mode='train'):
        self.hp = hp
        self.mode = mode
        self.split_ratio = hp.data.split_ratio
        self.random_seed = hp.data.random_seed
        self.criteria = [criterion.strip() for criterion in self.hp.data.criteria.split(",")]

        self.root = Path(self.hp.data.dataset_dir)

        label_json = self.hp.data.label_json_file
        with open(label_json, 'r', encoding='utf-8') as f:
            json_data = json.load(f)
        logger.info(json_data['categories'])
        self.image_list = json_data['items'].keys()

        if self.hp.data.use_face_crop:
            
            self.bbox_dict = {}
            
            front_dict = {}
            left_dict = {}
            right_dict = {}

            for image_name in self.image_list:
                if 'bbox' in json_data['items'][image_name].keys():
                    if image_name.split('/')[0] == 'front':
                        front_dict[image_name.split('/')[-1]] = json_data['items'][image_name]['bbox']
                    elif image_name.split('/')[0] == 'left':
                        left_dict[image_name.split('/')[-1]] = json_data['items'][image_name]['bbox']
                    elif image_name.split('/')[0] == 'right':
                        right_dict[image_name.split('/')[-1]] = json_data['items'][image_name]['bbox']
                    else:
                        raise ValueError(f"Invalid view: {image_name.split('/')[0]}")
                
                else:
                    logger.info(f"{image_name} have no bbox")
                    if image_name.split('/')[0] == 'front':
                        front_dict[image_name.split('/')[-1]] = None
                    elif image_name.split('/')[0] == 'left':
                        left_dict[image_name.split('/')[-1]] = None
                    elif image_name.split('/')[0] == 'right':
                        right_dict[image_name.split('/')[-1]] = None
                    else:
                        raise ValueError(f"Invalid view: {image_name.split('/')[0]}")

            self.bbox_dict['front'] = front_dict
            self.bbox_dict['left'] = left_dict
            self.bbox_dict['right'] = right_dict

        else:
            self.bbox_dict = None

        if self.hp.preprocess_mode.facewarp:
            if not self.hp.preprocess_mode.warp_use_gpu:
                self.landmark_dict = {}
                
                front_dict = {}
                left_dict = {}
                right_dict = {}

                for image_name in self.image_list:
                    if 'landmark' in json_data['items'][image_name].keys():
                        if image_name.split('/')[0] == 'front':
                            front_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                        elif image_name.split('/')[0] == 'left':
                            left_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                        elif image_name.split('/')[0] == 'right':
                            right_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                        else:
                            raise ValueError(f"Invalid view: {image_name.split('/')[0]}")
                    else:
                        logger.info(f"{image_name} have no landmark")
                        if image_name.split('/')[0] == 'front':
                            front_dict[image_name.split('/')[-1]] = None
                        elif image_name.split('/')[0] == 'left':
                            left_dict[image_name.split('/')[-1]] = None
                        elif image_name.split('/')[0] == 'right':
                            right_dict[image_name.split('/')[-1]] = None
                        else:
                            raise ValueError(f"Invalid view: {image_name.split('/')[0]}")
                
                self.landmark_dict['front'] = front_dict
                self.landmark_dict['left'] = left_dict
                self.landmark_dict['right'] = right_dict
            
            else:
                self.landmark_dict = 0

        else:
            self.landmark_dict = None

        self.category_list = json_data['categories']

        self.data = dict()
        self.front_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'front']
        self.image_label = [json_data['items'][image_name] for image_name in self.image_list if image_name.split('/')[0] == 'front']

        front_image_train, front_image_test, image_label_train, image_label_test =\
            train_test_split(self.front_image_list, self.image_label,
                             test_size=self.split_ratio, random_state=self.random_seed)
        self.data['front'] = {'train': front_image_train, 'test': front_image_test}
        self.data['label'] = {'train': image_label_train, 'test': image_label_test}

        if self.hp.model.use_multimodal:
            self.left_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'left']
            self.right_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'right']
            left_image_train, left_image_test, _, _ = \
                train_test_split(self.left_image_list, self.image_label,
                                 test_size=self.split_ratio, random_state=self.random_seed)
            right_image_train, right_image_test, _, _ = \
                train_test_split(self.right_image_list, self.image_label,
                                 test_size=self.split_ratio, random_state=self.random_seed)
            self.data['left'] = {'train': left_image_train, 'test': left_image_test}
            self.data['right'] = {'train': right_image_train, 'test': right_image_test}

        transform_list = get_transform(self.hp, mode=mode)
        self.transform = transforms.Compose(transform_list)


    def _load_image(self, path):
        if not path.exists():
            raise FileNotFoundError
        else:
            if self.hp.preprocess_mode.channels == 1:
                return ImageOps.exif_transpose(Image.open(path).convert('L'))  # read grayscale images
            elif self.hp.preprocess_mode.channels == 3:
                return ImageOps.exif_transpose(Image.open(path).convert("RGB"))
            else:  # FIXME: It may be unsafe. You may raise error.
                return ImageOps.exif_transpose(Image.open(path))

    def get_num_classes(self, criterion_name):
        try:
            return len(self.category_list[criterion_name])
        except KeyError:
            raise KeyError("One of skin_type, humidity, pore, blemish, acne, and symptom can be criterion")

    def get_config(self):
        return self.hp

    def get_dataset_mode(self):
        return self.mode

    def get_criteria(self):
        return self.criteria

    def __len__(self):
        return len(self.data['label'][self.mode])

    def face_crop(self, sample, name, view):
        if self.bbox_dict[view][name] is not None:
            return sample.crop(tuple(self.bbox_dict[view][name]))
        else:
            return sample

    def face_warp(self, sample, name, view):
        if self.landmark_dict == 0:
            warped = RandomWarp(sample, use_gpu=True)
            return warped
        else:
            if self.landmark_dict[view][name] is not None:
                landmark = self.landmark_dict[view][name]
                warped = RandomWarp(sample, landmark, use_gpu=False)
                return warped
            else:
                return sample


    def __getitem__(self, idx):
        image_sample = list()
        front_sample = self._load_image(self.data['front'][self.mode][idx])

        if self.landmark_dict is not None:
            front_sample = self.face_warp(front_sample, self.data['front'][self.mode][idx].name, 'front')
        if self.bbox_dict is not None:
            front_sample = self.face_crop(front_sample, self.data['front'][self.mode][idx].name, 'front')
        
        front_sample = self.transform(front_sample)
        image_sample.append(front_sample)
        labels = [self.data['label'][self.mode][idx][criterion] for criterion in self.criteria]        

        if self.hp.model.use_multimodal:
            left_sample = self._load_image(self.data['left'][self.mode][idx])

            if self.landmark_dict is not None:
                left_sample = self.face_warp(left_sample, self.data['left'][self.mode][idx].name, 'left')
            if self.bbox_dict is not None:
                left_sample = self.face_crop(left_sample, self.data['left'][self.mode][idx].name, 'left')

            left_sample = self.transform(left_sample)
            image_sample.append(left_sample)

            right_sample = self._load_image(self.data['right'][self.mode][idx])

            if self.landmark_dict is not None:
                right_sample = self.face_warp(right_sample, self.data['right'][self.mode][idx].name, 'right')
            if self.bbox_dict is not None:
                right_sample = self.face_crop(right_sample, self.data['right'][self.mode][idx].name, 'right')

            right_sample = self.transform(right_sample)
            image_sample.append(right_sample)
            
            return image_sample, labels

        else:
            return image_sample, labels



class PatchDataset(DatasetBack):
    def __init__(self, hp, mode='train'):
        self.hp = hp
        self.mode = mode
        self.split_ratio = hp.data.split_ratio
        self.random_seed = hp.data.random_seed
        if len(self.hp.data.criteria.split(',')) >= 2:
            raise ValueError(f"Patch Training does not support multiple criteria!")
        
        self.criterion = self.hp.data.criteria
        self.root = Path(self.hp.data.dataset_dir)

        label_json = self.hp.data.label_json_file
        with open(label_json, 'r', encoding='utf-8') as f:
            json_data = json.load(f)
        logger.info(json_data['categories'])
        logger.info(self.criterion)
        self.image_list = json_data['items'].keys()

        if not self.hp.preprocess_mode.kp_use_gpu:
            self.landmark_dict = {}
            
            front_dict = {}
            left_dict = {}
            right_dict = {}

            for image_name in self.image_list:
                if 'landmark' in json_data['items'][image_name].keys():
                    if image_name.split('/')[0] == 'front':
                        front_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                    elif image_name.split('/')[0] == 'left':
                        left_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                    elif image_name.split('/')[0] == 'right':
                        right_dict[image_name.split('/')[-1]] = np.array(json_data['items'][image_name]['landmark'])
                    else:
                        raise ValueError(f"Invalid view: {image_name.split('/')[0]}")
                else:
                    logger.info(f"{image_name} have no landmark")
                    if image_name.split('/')[0] == 'front':
                        front_dict[image_name.split('/')[-1]] = None
                    elif image_name.split('/')[0] == 'left':
                        left_dict[image_name.split('/')[-1]] = None
                    elif image_name.split('/')[0] == 'right':
                        right_dict[image_name.split('/')[-1]] = None
                    else:
                        raise ValueError(f"Invalid view: {image_name.split('/')[0]}")
            
            self.landmark_dict['front'] = front_dict
            self.landmark_dict['left'] = left_dict
            self.landmark_dict['right'] = right_dict
        
        else:
            self.landmark_dict = 0

        self.category_list = json_data['categories']
        classes = self.category_list[self.criterion].values()
        self.class_match_dict = {s : [] for s in classes}

        self.data = dict()
        self.front_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'front']
        self.image_label = [json_data['items'][image_name][self.criterion] for image_name in self.image_list if image_name.split('/')[0] == 'front']

        for ii, img in enumerate(self.front_image_list):
            self.class_match_dict[self.image_label[ii]].append(img.name)

        front_image_train, front_image_test, image_label_train, image_label_test =\
            train_test_split(self.front_image_list, self.image_label,
                             test_size=self.split_ratio, random_state=self.random_seed)
        
        self.data['front'] = {'train': front_image_train, 'test': front_image_test}
        self.data['label'] = {'train': image_label_train, 'test': image_label_test}

        if self.hp.model.use_multimodal:
            self.left_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'left']
            self.right_image_list = [self.root / image_name for image_name in self.image_list if image_name.split('/')[0] == 'right']
            left_image_train, left_image_test, _, _ = \
                train_test_split(self.left_image_list, self.image_label,
                                 test_size=self.split_ratio, random_state=self.random_seed)
            right_image_train, right_image_test, _, _ = \
                train_test_split(self.right_image_list, self.image_label,
                                 test_size=self.split_ratio, random_state=self.random_seed)
            self.data['left'] = {'train': left_image_train, 'test': left_image_test}
            self.data['right'] = {'train': right_image_train, 'test': right_image_test}

        transform_list = get_transform(self.hp, mode=mode)
        self.transform = transforms.Compose(transform_list)

        logging.info(f'Mode: {mode}; Dataset: {len(self.data["front"][mode])} imgs loaded.')


    def _load_image(self, path):
        if not path.exists():
            raise FileNotFoundError
        else:
            if self.hp.preprocess_mode.channels == 1:
                return ImageOps.exif_transpose(Image.open(path).convert('L'))  # read grayscale images
            elif self.hp.preprocess_mode.channels == 3:
                return ImageOps.exif_transpose(Image.open(path).convert("RGB"))
            else:  # FIXME: It may be unsafe. You may raise error.
                return ImageOps.exif_transpose(Image.open(path))

    def get_num_classes(self, criterion_name):
        try:
            return len(self.category_list[criterion_name])
        except KeyError:
            raise KeyError("One of skin_type, humidity, pore, blemish, acne, and symptom can be criterion")

    def get_config(self):
        return self.hp

    def get_dataset_mode(self):
        return self.mode

    def get_criteria(self):
        return self.criterion

    def __len__(self):
        return len(self.data['label'][self.mode])

    def get_patches(self, sample, view, name):
        if self.landmark_dict == 0:
            return generate_patches(sample, view, landmarks=None)
        else:
            if self.landmark_dict[view][name] is None:
                return generate_patches(sample, view, landmarks=None)
            else:
                return generate_patches(sample, view, landmarks=self.landmark_dict[view][name])


    def __getitem__(self, idx):

        random_num = random()

        image_sample = list()
        front_sample = self._load_image(self.data['front'][self.mode][idx])
        front_name = self.data['front'][self.mode][idx].name
        front_patches = self.get_patches(front_sample, 'front', front_name)
        front_patches = [self.transform(x) for x in front_patches]
        image_sample += front_patches

        label = self.data['label'][self.mode][idx]

        cur_candidates = self.class_match_dict[label].copy()
        cur_candidates.remove(self.data['front'][self.mode][idx].name)
        random_add = cur_candidates[choice(list(range(len(cur_candidates))))]

        ## Additional Patches
        additional_front = self._load_image(self.root / 'front' / random_add)
        rnd_front_patches = sample(self.get_patches(additional_front, 'front', random_add), int(len(front_patches)*random_num))
        rnd_front_patches = [self.transform(x) for x in rnd_front_patches]
        image_sample += rnd_front_patches        

        if self.hp.model.use_multimodal:
            left_sample = self._load_image(self.data['left'][self.mode][idx])
            left_name = self.data['left'][self.mode][idx].name
            left_patches = self.get_patches(left_sample, 'left', left_name)
            left_patches = [self.transform(x) for x in left_patches]
            image_sample += left_patches

            ## Additional Patches
            additional_left = self._load_image(self.root / 'left' / random_add)
            rnd_left_patches = sample(self.get_patches(additional_left, 'left', random_add), int(len(left_patches)*random_num))
            rnd_left_patches = [self.transform(x) for x in rnd_left_patches]
            image_sample += rnd_left_patches

            right_sample = self._load_image(self.data['right'][self.mode][idx])
            right_name = self.data['right'][self.mode][idx].name
            right_patches = self.get_patches(right_sample, 'right', right_name)
            right_patches = [self.transform(x) for x in right_patches]
            image_sample += right_patches

            ## Additional Patches
            additional_right = self._load_image(self.root / 'right' / random_add)
            rnd_right_patches = sample(self.get_patches(additional_right, 'right', random_add), int(len(right_patches)*random_num))
            rnd_right_patches = [self.transform(x) for x in rnd_right_patches]
            image_sample += rnd_right_patches
            
            return image_sample, [label]

        else:
            return image_sample, [label]
    
# def PatchCollateFn(batch):
#     image_sample, labels = batch
#     resampled = []
#     relabeled = []
#     for b in range(batch.size(0)):
#         cur_samples = image_sample[b]
#         cur_label = labels[b]
#         for patch in cur_samples:
#             resampled.append(patch)
#             relabeled.append(cur_label)
    
#     return torch.cat(resampled, dim=0), torch.cat(relabeled, dim=0)


def main():
    from omegaconf import OmegaConf
    from torch.utils.data import DataLoader
    configs_dir = '../configs/classification_patch.yaml'
    hp = OmegaConf.load(configs_dir)
    train_dataset = PatchDataset(hp, mode='train')
    train_loader = DataLoader(train_dataset, batch_size=hp.train.batch_size, num_workers=8)
    patches, labels = next(iter(train_loader))


if __name__ == '__main__':
    main()