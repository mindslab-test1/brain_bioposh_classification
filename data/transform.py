import torchvision.transforms as transforms
import numpy as np
import cv2
import random
import PIL.Image as Image
import math

from skimage.transform import PiecewiseAffineTransform, warp
from numpy.random import normal, uniform  
import sys
sys.path.insert(0, "..")
from face_alignment import api as face_alignment
from tqdm import tqdm
import torch

FA_MODEL = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D)

def get_transform(cf, mode='train'):
    transform_list = []

    interp_method = __resampling_filter(cf.preprocess_mode.interp_method)

    if __transform_apply_mode(cf.preprocess_mode.horizontal_flip, mode):
        transform_list.append(transforms.RandomHorizontalFlip())

    if __transform_apply_mode(cf.preprocess_mode.vertical_flip, mode):
        transform_list.append(transforms.RandomVerticalFlip())

    if __transform_apply_mode(cf.preprocess_mode.random_rotation, mode):
        transform_list.append(transforms.RandomRotation(15))

    if __transform_apply_mode(cf.preprocess_mode.gaussian_blur, mode):
        transform_list.append(transforms.Lambda(__gaussian_blur))

    if __transform_apply_mode(cf.preprocess_mode.salt_pepper, mode):
        transform_list.append(transforms.Lambda(__salt_and_pepper))
    
    if __transform_apply_mode(cf.preprocess_mode.perspective, mode):
        transform_list.append(transforms.Lambda(__perspective_transform))

    if __transform_apply_mode(cf.preprocess_mode.color_jitter, mode):
        transform_list.append(transforms.ColorJitter(brightness=0.5,
                                                     contrast=0.5,
                                                     saturation=0,
                                                     hue=0))


    if __transform_apply_mode(cf.preprocess_mode.resize, mode):
        transform_list.extend([transforms.Resize((128, 128), interpolation=interp_method),
                          transforms.ToTensor()])
    else:
        transform_list.extend([transforms.ToTensor()])

    if __transform_apply_mode(cf.preprocess_mode.normalize, mode):
        if cf.preprocess_mode.channels == 1:
            transform_list.append(transforms.Normalize([0.5], [0.5]))
        else:
            transform_list.append(transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]))

    return transform_list


def __transform_apply_mode(cf_value, mode):
    if cf_value.upper() == "ALL" or mode.upper() in cf_value.upper():
        return True
    else:
        return False


def __resampling_filter(method='bicubic'):
    if 'bicubic' in method:
        return Image.BICUBIC
    elif 'bilinear' in method:
        return Image.BILINEAR
    elif 'lanczos' in method:
        return Image.LANCZOS
    elif 'nearest' in method:
        return Image.NEAREST
    else:
        raise Exception("There is no interpolation method but for nearest, lanczos, bilinear, bicubic")


def __gaussian_blur(img):
    img = np.array(img)
    random_kernel = random.choice([1, 3])
    img = cv2.GaussianBlur(img, (random_kernel, random_kernel), 0) #img, 커널크기, 표준편
    img = Image.fromarray(img)
    return img


def get_landmarks(image):
    rm_landmark_list = [1, 2, 4, 5, 7, 15, 14, 12, 11, 9, 49, 51, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 19, 25, 28, 29, 33, 40, 47]

    with torch.no_grad():
        landmark = FA_MODEL.get_landmarks(image)
    if landmark is None:
        return None
    else:
        landmark_final = rm_landmarks(landmark[0], rm_landmark_list)
        landmark = np.array(landmark_final)
        # landmark *= scale

        return landmark

def rm_landmarks(landmark, rm_list):
    landmark = landmark.tolist()
    for i in sorted(rm_list, reverse=True):
        del landmark[i]

    return landmark

def clean_landmarks(landmark):
    rm_landmark_list = [1, 2, 4, 5, 7, 15, 14, 12, 11, 9, 49, 51, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 19, 25, 28, 29, 33, 40, 47]
    landmark_final = rm_landmarks(landmark, rm_landmark_list)
    landmark = np.array(landmark_final)
    # landmark *= scale

    return landmark

def __random_warp(image, landmark=None, use_gpu=False):
    distribution = 'Uniform'
    warp_scale = 3.0
    warp_min = 0.1

    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)

    if use_gpu:
        landmarks = get_landmarks(image)
    else:
        landmarks = clean_landmarks(landmark)

    if landmarks is None:
        #When No faces were detected.
        return Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    else:
        landmarks_ls = landmarks.tolist()

        if distribution == 'Normal':
            noisy_landmarks = landmarks.copy() + np.random.normal(0, warp_scale, landmarks.shape) + warp_min
        elif distribution == 'Uniform':
            noisy_landmarks = landmarks.copy() + np.random.uniform(-warp_scale - warp_min, warp_scale, landmarks.shape) + warp_min

        image_corners = np.array([[0,0], [image.shape[1], 0], [0, image.shape[0]], [image.shape[1], image.shape[0]]])

        transform_points = np.append(landmarks, image_corners, axis=0)
        noisy_transform_points = np.append(noisy_landmarks, image_corners, axis=0)

        transform = PiecewiseAffineTransform()
        transform.estimate(noisy_transform_points, transform_points)

        warped = (warp(image, transform, output_shape=image.shape)*255).astype(np.uint8)

        output = Image.fromarray(cv2.cvtColor(warped, cv2.COLOR_BGR2RGB))
        
        return output

def __salt_and_pepper(image):
    s_vs_p = 0.5
    amount = 0.004
    image = np.array(image)
    out = np.copy(np.array(image))

    # Salt mode
    num_salt = np.ceil(amount * (image.size) * s_vs_p)
    coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
    out[coords] = 1

    # Pepper mode
    num_pepper = np.ceil(amount * (image.size) * (1. - s_vs_p))
    coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
    out[coords] = 0

    out = Image.fromarray(out)
    return out

def __perspective_transform(image):
    image = np.array(image)
    h, w, c = image.shape
    out = image.copy()

    cnt = np.array([[0, 0], [w-1, 0], [0, h-1], [w-1, h-1]])

    dst_pts = cnt.astype(np.float32)
    src_pts = __pts_augmentation(w, h)

    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warped = cv2.warpPerspective(out, M, (w, h))

    out = Image.fromarray(warped)
    return out

def __pts_augmentation(pt1, pt2):
    max_margin_pt1 = int(pt1 // (math.pow(10, len(str(pt1))-1))) ** 4
    max_margin_pt2 = int(pt2 // (math.pow(10, len(str(pt2))-1))) ** 4

    tl = [random.randint(0, max_margin_pt1), random.randint(0, max_margin_pt2)]
    tr = [pt1 + random.randint(-max_margin_pt1, 0), random.randint(0, max_margin_pt2)]
    bl = [random.randint(0, max_margin_pt1), pt2 + random.randint(-max_margin_pt2, 0)]
    br = [pt1 + random.randint(-max_margin_pt1, 0), pt2 + random.randint(-max_margin_pt2, 0)]

    return np.array([tl, tr, bl, br], dtype='float32')

###########ORIGINAL###########
# def __perspective_transform(image):
#     image = np.array(image)
#     h, w, c = image.shape

#     cnt = np.array([[0, 0], [w-1, 0], [0, h-1], [w-1, h-1]])
#     rec = cv2.minAreaRect(cnt)
#     print(rec)

#     angle = rec[2]
#     Mr = cv2.getRotationMatrix2D((w/2, h/2), angle, 1)
#     rec0 = (rec[0], rec[1], 0.0)

#     box = cv2.boxPoints(rec0)
    
#     box = np.int0(cv2.transform(np.array([box]), Mr))[0]
#     box[box < 0] = 0
#     new_w, new_h = [box[1][0] - box[0][0], box[-1][1] - box[0][1]]

#     width, height = __pts_augmentation(new_w, new_h)

#     src_pts = box.astype(np.float32)
#     dst_pts = np.array([[0, height - 1], [0, 0], [width - 1, 0], [width - 1, height - 1]], dtype='float32')

#     M = cv2.getPerspectiveTransform(src_pts, dst_pts)
#     warped = cv2.warpPerspective(image, M, (width, height))
#     print(warped.shape)
#     output = Image.fromarray(warped)
#     return output

# def __pts_augmentation(pt1, pt2):
#     # if pt1, pt2 = (342, 174) , max_marign_pt1, max_margin_pt2 = (3, 1).
#     max_margin_pt1 = int(pt1 // (math.pow(10, len(str(pt1))-1))) * 2
#     max_margin_pt2 = int(pt2 // (math.pow(10, len(str(pt2))-1))) * 2
#     # print("before pt1, pt2 : ({}, {})".format(pt1, pt2))
#     pt1 += random.randint(-max_margin_pt1, 0)
#     pt2 += random.randint(-max_margin_pt2, 0)
#     # print("after pt1, pt2 : ({}, {})".format(pt1, pt2))
#     return pt1, pt2
