from pathlib import Path
import datetime
import logging

from omegaconf import OmegaConf
from argparse import ArgumentParser

from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers
from pytorch_lightning.callbacks import ModelCheckpoint

from torch.utils.data import DataLoader
# from torch.multiprocessing import set_start_method

from models import MultiTaskNet
from data import BioposhDataset, PatchDataset

import warnings

warnings.filterwarnings(action='ignore')

def main(args):
    # load config file
    hp = OmegaConf.load(args.configs)

    # if hp.preprocess_mode.facewarp.lower() != 'none':
    #     set_start_method('spawn')

    # dataset load
    if hp.mode == 'whole':
        train_dataset = BioposhDataset(hp, mode='train')
    
        if (hp.preprocess_mode.facewarp.upper() == "ALL" or 'TRAIN' in hp.preprocess_mode.facewarp.upper()) and hp.preprocess_mode.warp_use_gpu:
            num_worker = 0
            pin_memory = False
        else:
            num_worker = hp.train.workers
            pin_memory = hp.train.pin_memory
        
        train_loader = DataLoader(train_dataset, batch_size=hp.train.batch_size, num_workers=num_worker, pin_memory=pin_memory)
        criteria_list = [criterion.strip() for criterion in hp.data.criteria.split(",")]
        num_classes = [train_dataset.get_num_classes(criterion) for criterion in criteria_list]

        valid_dataset = BioposhDataset(hp, mode='test')

        if (hp.preprocess_mode.facewarp.upper() == "ALL" or 'TEST' in hp.preprocess_mode.facewarp.upper()) and hp.preprocess_mode.warp_use_gpu:
            num_worker = 0
            pin_memory = False
        else:
            num_worker = hp.train.workers
            pin_memory = hp.train.pin_memory
        valid_loader = DataLoader(valid_dataset, batch_size=hp.test.batch_size, num_workers=num_worker, pin_memory=pin_memory)
    
    elif hp.mode == 'patch':
        train_dataset = PatchDataset(hp, mode='train')
        num_worker = hp.train.workers
        pin_memory = hp.train.pin_memory
    
        train_loader = DataLoader(train_dataset, batch_size=hp.train.batch_size, num_workers=num_worker, pin_memory=pin_memory)
        criteria_list = [criterion.strip() for criterion in hp.data.criteria.split(",")]
        num_classes = [train_dataset.get_num_classes(criterion) for criterion in criteria_list]

        valid_dataset = PatchDataset(hp, mode='test')
        valid_loader = DataLoader(valid_dataset, batch_size=hp.test.batch_size, num_workers=num_worker, pin_memory=pin_memory)

    # load main model
    model = MultiTaskNet(hp, criteria_list=criteria_list, num_classes=num_classes)

    # set checkpoint dir
    checkpoint_root_dir = Path(hp.data.weight_dir)
    checkpoint_save_dir = checkpoint_root_dir / args.name
    checkpoint_save_dir.mkdir(parents=True, exist_ok=True)

    # set checkpoint saving policy
    checkpoint_callback = ModelCheckpoint(
        dirpath=checkpoint_save_dir,
        monitor='val/accuracy',
        verbose=True
    )

    # set trainer
    trainer = Trainer(
        logger=pl_loggers.TensorBoardLogger(hp.tensorboard.run_dir),
        checkpoint_callback=checkpoint_callback,
        weights_save_path=checkpoint_save_dir,
        gpus=0,
        num_sanity_val_steps=1,
        resume_from_checkpoint=args.checkpoint_path,
        progress_bar_refresh_rate=1,
        max_epochs=100,
    )

    trainer.fit(model, train_loader, valid_loader)  # due to limited data, we substitue validation dataset to test dataset.



def get_args():
    parser = ArgumentParser()

    # name
    parser.add_argument('-n', '--name', type=str,
                        default=datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S'), help="task name")
    parser.add_argument('-c', '--configs', type=str, default='./configs/classification.yaml')
    parser.add_argument('-l', '--log_level', choices=['DEBUG', 'INFO'], default='INFO')
    parser.add_argument('-p', '--checkpoint_path', type=str, default=None,
                        help="path of checkpoint for resuming")

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    bioposh_logger = logging.getLogger('bioposh')

    main(args)