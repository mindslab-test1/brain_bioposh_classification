from torch.utils.tensorboard import SummaryWriter
import os
import shutil


class Writer(SummaryWriter):
    def __init__(self, cf, name):
        self.cf = cf
        self.use_tensorboard = self.cf.tensorboard.use_tensorboard
        if self.use_tensorboard:
            self.tensorboard = SummaryWriter(os.path.join(cf.tensorboard.run_dir, name))

        shutil.copy("configs/classification.yaml", os.path.join(cf.tensorboard.run_dir, name, "config.yaml"))

    def train_loss_logging(self, log_name, train_loss, step):
        if self.use_tensorboard:
            self.tensorboard.add_scalar('Train/{}/loss'.format(log_name), train_loss, step)

    def validation_loss_logging(self, log_name, test_loss, step):
        if self.use_tensorboard:
            self.tensorboard.add_scalar('Val/{}/loss'.format(log_name), test_loss, step)

    def train_acc_logging(self, log_name, acc ,step):
        if self.use_tensorboard:
            self.tensorboard.add_scalar('Train/{}/acc'.format(log_name), acc, step)

    def validation_acc_logging(self, log_name, acc, step):
        if self.use_tensorboard:
            self.tensorboard.add_scalar('Val/{}/acc'.format(log_name), acc, step)

    def train_images_logging(self, img_name, train_imgs, step):
        if self.use_tensorboard:
            self.tensorboard.add_images('Train/{}/images'.format(img_name), train_imgs, step)

    def validation_images_logging(self, img_name, test_imgs, step):
        if self.use_tensorboard:
            self.tensorboard.add_images('Val/{}/images'.format(img_name), test_imgs, step)

    def train_image_logging(self, img_name, train_img, step):
        if self.use_tensorboard:
            self.tensorboard.add_image('Train/{}/image'.format(img_name), train_img, step)

    def validation_image_logging(self, img_name, test_img, step):
        if self.use_tensorboard:
            self.tensorboard.add_image('Val/{}/image'.format(img_name), test_img, step)
