import torch
import torch.nn as nn
from .modules import get_feature_extractor, get_classification_head
class Classifier(nn.Module):
    def __init__(self, hp, num_classes):
        super(Classifier, self).__init__()
        self.hp = hp
        self.use_multimodal = hp.model.use_multimodal

        self.feature_extractor, cls_in_features = get_feature_extractor(
            self.hp.model.backbone)
        # self.feature_extractor.eval()

        if self.use_multimodal:  # Assume that we know there are 3 images as input
            cls_in_features *= 3

        if isinstance(num_classes, list):
            self.cls_heads = nn.ModuleList([get_classification_head(cls_in_features, num_class)
                                            for num_class in num_classes])

        elif isinstance(num_classes, int):
            self.cls_heads = nn.ModuleList(
                [get_classification_head(cls_in_features, num_classes)])
            


    def forward(self, input_list):
        if self.use_multimodal:
            features = [self.feature_extractor(x) for x in input_list]
            features = torch.cat(features, dim=1)
        else:
            features = self.feature_extractor(input_list[0])

        features = torch.flatten(features, 1)
        
        outputs = []
        for cls_head in self.cls_heads:
            outputs.append(cls_head(features))
        return outputs


class PatchClassifier(nn.Module):
    def __init__(self, hp, num_classes):
        super(PatchClassifier, self).__init__()
        self.hp = hp
        self.bin_size = hp.model.bin_size

        self.feature_extractor, cls_in_features = get_feature_extractor(
            self.hp.model.backbone, pretrained=hp.model.use_pretrained)
        # self.feature_extractor.eval()

        self.interm_heads = get_classification_head(cls_in_features, self.bin_size)

        self.cls_head = get_classification_head(self.bin_size, num_classes[0])

    def forward(self, input_list):
        batch_size = input_list[0].size(0)
        num_patch = len(input_list)
        x = torch.cat(input_list, dim=0)
        features = self.feature_extractor(x)
        features = self.interm_heads(torch.flatten(features, 1))
        
        features = list(torch.split(features, batch_size))
        features = torch.cat([feature.unsqueeze(1) for feature in features], dim=1)

        fusion = torch.sum(features, dim=1)
        fusion = nn.LayerNorm(fusion.size()[1:])(fusion)
        output = self.cls_head(fusion)
        return [output]