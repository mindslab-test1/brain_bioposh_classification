import os

opj = os.path.join


def save_model(model, savedir, category_num, optimizer, architecture):
    if not os.path.isdir(savedir):
        os.mkdir(savedir)

    model = model.to(torch.device('cpu'))
    torch.save(
        {'model_state_dict': model.state_dict(),
         'category_num': category_num,
         'optimizer': optimizer,
         'model': architecture},
        opj(savedir, 'best_model.pth')
    )


def load_checkpoint(model, checkpoint):
    model = model.to(torch.device('cpu'))
    model.load_state_dict(checkpoint)
    model = model.cuda()
    return model
