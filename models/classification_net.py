import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.optim import Adam, lr_scheduler

import pytorch_lightning as pl
import torchmetrics

from .classifier import Classifier, PatchClassifier

class MultiTaskNet(pl.LightningModule):
    def __init__(self, hp, criteria_list, num_classes):
        super(MultiTaskNet, self).__init__()
        self.hp = hp
        self.save_hyperparameters(self.hp)
        
        if hp.mode == 'whole':
            self.classifier = Classifier(self.hp, num_classes)
        elif hp.mode == 'patch':
            self.classifier = PatchClassifier(self.hp, num_classes)
        else:
            raise ValueError(f"Invalid mode: {hp.mode}")

        self.losses = dict()
        for criterion in criteria_list:
            self.losses.update({criterion: nn.CrossEntropyLoss()})
        
        self.metric_acc = dict()
        for criterion in criteria_list:
            self.metric_acc.update({criterion: torchmetrics.Accuracy()})

    def forward(self, images):
        results = self.classifier(images)
        return results

    def training_step(self, batch, batch_idx):
        images, gt_list = batch
        results = self.classifier(images)
        losses = [loss_func(result, gt) for result, gt, loss_func in zip(results, gt_list, self.losses.values())]

        total_loss = sum(losses)
        self.log('train/total_loss', total_loss, on_step=False, on_epoch=True)
        for criterion, loss_value in zip(self.losses.keys(), losses):
            self.log(f'train/{criterion}', loss_value, on_step=False, on_epoch=True)

        return total_loss

    def validation_step(self, batch, batch_idx):
        images, gt_list = batch
        results = self.classifier(images)
        
        losses = [loss_func(result, gt) for result, gt, loss_func in zip(results, gt_list, self.losses.values())]
        accs = [calculate(result.softmax(dim=-1), gt) for result, gt, calculate in zip(results, gt_list, self.metric_acc.values())]

        total_loss = sum(losses)
        total_acc = sum(accs) / len(accs)
        self.log('val/total_loss', total_loss, on_step=False, on_epoch=True)
        self.log('val/total_acc', total_acc, on_step=False, on_epoch=True)
        for criterion, loss_value, acc_value in zip(self.losses.keys(), losses, accs):
            self.log(f'val/{criterion}_loss', loss_value, on_step=False, on_epoch=True)
            self.log(f'val/{criterion}_acc', acc_value, on_step=False, on_epoch=True)


        return total_loss
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, self.classifier.parameters()), lr=self.hp.train.lr.initial)
        scheduler = lr_scheduler.StepLR(optimizer, step_size=self.hp.train.lr.decay.epoch, gamma=self.hp.train.lr.decay.gamma)
        return [optimizer], [scheduler]