import torchvision.models as models
import torch.nn as nn
import torch

def get_feature_extractor(name, pretrained=True):
    if name.upper() == 'RESNET50':
        model = models.resnet50(pretrained=pretrained)
        num_features = model.fc.in_features
    elif name.upper() == 'RESNET101':
        model = models.resnet101(pretrained=pretrained)
        num_features = model.fc.in_features
    elif name.upper() == 'VGG16':
        model = models.vgg16(pretrained=pretrained)
        num_features = model.classifier[0].in_features
    elif name.upper() == 'VGG19':
        model = models.vgg19(pretrained=pretrained)
        num_features = model.classifier[0].in_features
    else:
        raise NotImplementedError("{} is not supported yet".format(name))

    model = nn.Sequential(*list(model.children())[:-1])
    return model, num_features


def get_classification_head(in_features, out_features):
    fc_layer = nn.Linear(in_features, out_features)
    return fc_layer
