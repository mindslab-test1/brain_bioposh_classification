# Bioposh multimodal-multitask

This repository is heavily based on [`brain_image_classification`](https://pms.maum.ai/bitbucket/users/acasia_mindslab.ai/repos/brain_image_classification/browse)  
Author:  
- Hyoung-Kyu Song (@hksong35@mindslab.ai)
- Seung-Min Yang (@myaowng2@mindslab.ai)

## TODO

- Module Separation  


## FIXME

- When training using segments, process is killed


## Author's Directory Structures
printed with `tree -d -L 2`

```bash
.
|-- configs
|-- data
|   `-- __pycache__
|-- datasets -> /DATA/bioposh
|-- examples
|   `-- pretrained
|-- face_alignment
|   |-- __pycache__
|   `-- detection
|-- models
|   `-- __pycache__
|-- runs
|   |-- 2021-02-24
|   |-- 2021-02-25
|   |-- 2021-02-25_01:57:50
|   |-- 2021-02-25_04:11:12
|   |-- 2021-02-25_05:51:04
|   |-- 2021-02-26_03:10:07
|   `-- 2021-02-26_07:10:05
|-- visualize
|   `-- __pycache__
`-- weights
```

## Usages

See [`configs/classification.yaml`](./configs/classification.yaml) .

### Dataset 구성

Single `.json` 으로 작동하도록 구성하였습니다.
Dataset(`/DATA2/brain-vision-nas/ClientData/bioposh`) 확인해보시면 빠를 것 같습니다.

## Docker
### Debug
```bash
# build
docker build -t bioposh:v0.1.0 .
# run
docker run --ipc=host -it --gpus="all" -v /path/your/data:/path/your/data -v /path/your/workdir:/workspace --name hksong_bioposh bioposh:v0.1.0
```
